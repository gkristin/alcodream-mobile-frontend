(function($){	   
	/*
	*	BEGIN Catalog
	*/
	var $body = $('body');
	
	var ajaxRequest = null;
	
	var isStrgSup = !!window.localStorage;
    var data = isStrgSup ? window.localStorage.expandData : {};
    if(!data) {
        data = {};
    }

    if(typeof data === 'string') {
        data = $.parseJSON(data);
    } 
	
	var setStorage = function() {
        var newData = {};
        $('[data-expand-id]').each(function() { 
            newData[$(this).data('expandId')] = $(this).hasClass('shown') ? '1' : '0';
        });

        data = newData;

        if(isStrgSup) {
            window.localStorage.expandData = JSON.stringify(newData);
        }
    }

    var expandTree = function($el) {
        var $this = $el
        .toggleClass('shown');

        var $els = $this.parent().find('.level-2');
        
        if(!$this.hasClass('shown')) {
            $this.text('[+]');
			$els.fadeOut().addClass('hidden');           
        } else {
            $this.text('[-]');
			$els.fadeIn().removeClass('hidden');           
        }
    }

    var initStorage = function() { 
        $('[data-expand-id]').each(function() {
            var key = $(this).data('expandId');
            if(data[key] == '1') {
                expandTree($(this));
            }
        });
    }

    var removeExpandBtn = function($el) {
        $el.find('[data-filter-name="prop_region"] .level-1').filter(function(){
            return !$(this).find('.level-2').length;            
        })
        .children('.filter__sub-expand')      
        .remove()
    }
	
	var updateFilterCatalog = function(form) {		
		
		ajaxRequest = $.ajax({
			url: form.data('url'),				
			type: 'get',
			dataType: 'html',
			data: form.serialize(),
			success: function(html) {
				
				var $parseHTMLJ = $(html);
				
				$parseHTMLJ.find('[data-expand-id]').each(function() {
					
					var key = $(this).data('expandId');
					if(data[key] == '1') {
						expandTree($(this));
					}
				});

				removeExpandBtn($parseHTMLJ);
				
				$('.c-filter-wrapper').empty().append($parseHTMLJ);
				
				$body.removeClass('filter-active');
				$('.c-fixed').addClass('fixed');
				$('.c-filter').addClass('form--filter-fixed');				
				initSlider();
				
				var $el = $('.c-filter').find('.c-filter-section[data-filter-name="' + lastGroupId + '"]').prev();

				if(!$el.length) {
					$el = $('.c-filter');
				}

				var offset = $el.offset();
				console.log($el, offset);

				$('html,body').animate({scrollTop: offset.top - 214});
				
				// if(offset && ($(window).scrollTop() > offset.top - 60 + $el.height())) {
					// $('html,body').animate({scrollTop: offset.top - 60});
				// }
				
			},
			complete: function() {
				ajaxRequest = null;
			}
		});
	}
	
	
	// + Ещё
	$body.on('click', '.c-filter-add', function(e) {
		e.preventDefault();
		
		var $link = $(this);
		var $filterBlock = $link.closest('.c-filter-block');
		
		$link.toggleClass('active');
		
		if(!$link.hasClass('active')) {
			$filterBlock.find('.c-filter-hidden').addClass('hidden');			
		} else {
			$filterBlock.find('.c-filter-hidden').removeClass('hidden');
		}
	});
	
	// [+]Субрегионы
	$body.on('click', '.filter__sub-expand', function(e) {	
		e.preventDefault();
		expandTree($(this));       
		setStorage();
	});			
	
	//Изменение чекбокса
	var lastGroupId = null;
	$body.on('change', '.check__filter-field', function() {
		var $check = $(this);

		/** TODO: передавать параметры для скролла */
		lastGroupId = $(this).closest('.c-filter-section').data('filterName');
		
			
		var parentBl =  $(this).parent();
		if(!parentBl.hasClass('level-1')) {					
			var parentLevel1 = $(this).closest('.level-1');
			parentLevel1.find('.check__filter-field-parent').attr('checked', false);			
		}
		
		$(this).closest('.c-filter-section').find('.check__filter-any-field').prop('checked', false);
		
		updateFilterCatalog($(this).closest('form'))
		return false;
		
	});			
	
	// Изменение range 			
	$body.on('click', '.check__filter-range', function() {
		var $check = $(this);
		
		var	from = $(this).data('from'),
			to = $(this).data('to');

		$(this).closest('.c-filter-section').find('.c-minPrice').val(from);
		$(this).closest('.c-filter-section').find('.c-maxPrice').val(to);
		
		updateFilterCatalog($(this).closest('form'))

		return false;			
	});
	
	
	//Чекбокс для любых значений
	$body.on('click', '.check__filter-any-field', function() {
		if (!$(this).attr(':checked')) {
			$(this).closest('.c-filter-section').find('.check__filter-field').each(function() {
				$(this).prop('checked', false);
			});

			$(this).prop('checked', true);
			lastGroupId = $(this).closest('.c-filter-section').data('filterName');
			updateFilterCatalog($(this).closest('form'))
		}
	});
	
	
	$body.on('submit', '#filter-catalog', function() {			
		
		if (ajaxRequest != null) {
			ajaxRequest.abort();
		}

		var data = $(this).serializeArray();
		var url = $(this).attr('action');

		ajaxRequest = $.ajax({
			url: url,				
			type: 'get',
			dataType: 'html',
			data: data,
			success: function(html) {
				
				var $parseHTMLJ = $(html);
				
				$parseHTMLJ.find('[data-expand-id]').each(function() {
					
				var key = $(this).data('expandId');
					if(data[key] == '1') {
						expandTree($(this));
					}
				});

				removeExpandBtn($parseHTMLJ);

				$('.c-content-catalog-wrap').empty().append($parseHTMLJ);
				$body.removeClass('filter-active');
				initSlider();
				initSelect();
			},
			complete: function() {
				ajaxRequest = null;
			}
		});

		return false;
	});
	
	
	
	/*
	*	END Catalog
	*/	
    $(function(){		
        initStorage();
        removeExpandBtn($(document));
    });
})( jQuery );