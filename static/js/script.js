function PopUpShow(body){
	body.addClass('open-popup');
}

function PopUpHide(body){
	body.removeClass('open-popup');
}

function MenuLevel2Show (linkMenu, navLevels, navLevel, hideMenu) {
	var $this = linkMenu;
	var $thisData = $this.data('type');
	var $thisMenu = $('#' + $thisData); 		
	
	hideMenu.hide();
	$thisMenu.show();
	
	$('body').addClass('nav-level2');
	navLevels.css('height', navLevel.height());	
}

function MenuLevel3Show (linkMenu, navLevels, navLevel, hideMenu) {	
	var $this = linkMenu;
	var $thisData = $this.data('type');
	var $thisMenu = $('#' + $thisData);	
	
	hideMenu.hide();
	$thisMenu.show();
	
	$('body').addClass('nav-level3');
	navLevels.css('height', navLevel.height());		
}


function MenuLevel2Close(navLevels) {		
	$('body').removeClass('nav-level2');
	navLevels.css('height', 'auto');
}

function MenuLevel3Close(navLevels, navLevel) {		
	$('body').removeClass('nav-level3');	
	navLevels.css('height', navLevel.height());	
}


var validatorLanguage = {
	errorTitle: 'Ошибка отправки формы!',
	requiredFields: 'Необходимо заполнить поле',
	badTime: 'Введите корректное время',
	badEmail: 'Введите корректный e-mail адрес',
	badTelephone: 'Введите корректный номер телефона',
	badSecurityAnswer: 'Ответ на секретный вопрос не верен',
	badDate: 'Введите корректную дату',
	lengthBadStart: 'Значение должно быть между ',
	lengthBadEnd: ' знаками',
	lengthTooLongStart: 'Значение поля больше, чем ',
	lengthTooShortStart: 'Значение поля меньше, чем ',
	notConfirmed: 'Input values could not be confirmed',
	badDomain: 'Введите корректное доменное имя',
	badUrl: 'Значение поля не является корректным url-адресом',
	badCustomVal: 'Зачение поля не верно',
	andSpaces: ' и пробелов ',
	badInt: 'Поле может содержать только цифры',
	badSecurityNumber: 'Your social security number was incorrect',
	badUKVatAnswer: 'Incorrect UK VAT Number',
	badStrength: 'The password isn\'t strong enough',
	badNumberOfSelectedOptionsStart: 'You have to choose at least ',
	badNumberOfSelectedOptionsEnd: ' answers',
	badAlphaNumeric: 'Поле может содержать только буквы ',
	badAlphaNumericExtra: ' и ',
	wrongFileSize: 'Максимальный размер файла %s',
	wrongFileType: 'Разрешениы следующие форматы файлов: %s',
	groupCheckedRangeStart: 'Выберите между ',
	groupCheckedTooFewStart: 'Выберите не меньше, чем ',
	groupCheckedTooManyStart: 'Выберите не больше, чем ',
	groupCheckedEnd: ' item(s)',
	badCreditCard: 'The credit card number is not correct',
	badCVV: 'The CVV number was not correct',
	wrongFileDim: 'Incorrect image dimensions,',
	imageTooTall: 'the image can not be taller than',
	imageTooWide: 'the image can not be wider than',
	imageTooSmall: 'the image was too small',
	min: 'мин.',
	max: 'макс.',
	imageRatioNotAccepted: 'Image ratio is not accepted'
};

function initCustomValidate(){
	$.validate({
		validateOnBlur: false,
		errorMessagePosition: 'bottom',
		scrollToTopOnError: false,
		language: validatorLanguage,
		borderColorOnError: '#e41919',
		onSuccess : function($form){			
	    	//send form by ajax
				
			// if($form.hasClass('c-ajaxForm')){
				// sendFormbyAjax($form);

				// return false;
			// }
			
	    }
	});
}	

function showAccordion() {	
	$('body').on('click', '.c-accordion-link',  function (e) {			
		e.preventDefault();
		
		var $link = $(this),
			$accordion = $link.closest('.c-accordion'),
			$drops = $accordion.find('.c-accordion-drop'),
			$links = $accordion.find('.c-accordion-link');

		var $elem = $(this),
			$currDrop = $elem.next('.c-accordion-drop');					
		
		$elem.toggleClass('active');
		
		if ($elem.hasClass('active')) {				
			$currDrop.slideToggle(function () {
				$('html, body').stop().animate({ scrollTop: $elem.offset().top - 214 });					
			});
		} else {
			$currDrop.slideUp();
		}				
	});			
}

function goCategoryAnchors($categoryLinks) {
	$.each($categoryLinks, function () {
		var $link = $(this);
		
		$link.on('click', function () {
			
			var $elem = $(this);
			$elem.toggleClass('active');
			
			var $link = $('a[name="' + $elem.attr('href').replace('#', '') + '"]');
			console.log($link, $link.offset());
			
			if($elem.hasClass('active')) {					
				$('html, body').stop().animate({ scrollTop: $link.offset().top  - 224});		
			}
			
			$categoryLinks.not($elem).removeClass('active');
			
		});
	});
}

function initSlider() {
	$('body').find('#price-slider').each(function() {
		var slider = $(this),
			min = parseInt(slider.attr("data-min-limit")),
			minval = parseInt(slider.attr("data-min")),
			max = parseInt(slider.attr("data-max-limit")),
			maxval = parseInt(slider.attr("data-max")),
			minInput = $("input#minPrice"),
			maxInput = $("input#maxPrice"),
			delay = (max - min) / 1000;

		minInput.on('change keyup', function() {
			var value = parseInt($(this).val());

			if (!isNaN(value)) {
				slider.slider('values', 0, value);
			}
		});

		maxInput.on('change keyup', function() {
			var value = parseInt($(this).val());

			if (!isNaN(value)) {
				slider.slider('values', 1, value);
			}
		});

		slider.slider({
			range: true,
			min: min,
			max: max,
			values: [ minval, maxval ],
			slide: function( event, ui ) {
				if(!isNaN(delay) && ui.values[1] - ui.values[0] < delay){
					return false;
				}
				minInput.val(ui.values[0]);
				maxInput.val(ui.values[1]);
			},
			stop: function( event, ui ) {
				minInput.change();
			},
			create: function( event, ui ) {
				minInput.val(minval);
				maxInput.val(maxval);				
			}
		}); 
	});
}

function initSelect(){
	if($('.c-select').length) {
		$('.c-select').SumoSelect();
	}	
}

function sliceText(str, count){
 return str.slice(0, count);
}

function initInputMask() {
	$('.c-inputdate').inputmask("d.m.y");
}

function limitedText(elem, count) {
	elem.each(function(){
		var elem = $(this);		

	    if (elem.text().length > count){
			elem.text(sliceText(elem.text(), count)+'...');
		}
	});
}

function openOrder() {
	$('body').on('click', '.c-order-link', function(e) {
		e.preventDefault();
		
		var currenLink = $(this);
		var orderBl = currenLink.closest('.c-order');
		
		currenLink.toggleClass('active');
		
		(currenLink.hasClass('active')) ? orderBl.addClass('open') : orderBl.removeClass('open');		
	});
}

function updateOrders() {
	$('body').on('submit', '#filter-orders', function() {
		var data = $(this).serializeArray();
		var url = $(this).attr('action');
		
		
		$.ajax({
			url: url,				
			type: 'get',
			dataType: 'html',
			data: data,
			success: function(html) {
				var $parseHTMLJ = $(html);
				
				$('.c-content-orders-wrap').empty().append($parseHTMLJ);
				$('body').removeClass('filter-active');
				initSlider();
				initSelect();
				initInputMask();
			},
			complete: function() {
				
			}
		});
		return false;		
	});	
}



$(function () { 
	var $body = $('body');	
	
	//Скрыть PopUp при загрузке страницы    
    PopUpHide($body);
	
	// init owlCarousel
	$('.c-carousel').owlCarousel({
		center: true,
		items:1,
		loop:true,		
		responsive:{
			100:{
				items:1
			}
		}
	});
	
	// init slick carousel
	$('.c-product').slick({
		infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: "<span class='slideprev sprite a9'></span>",
        nextArrow: "<span class='slidenext sprite a8'></span>"
	});
	
	
	// price-slider	
	initSlider();
	
	//validate
	initCustomValidate();
	
	//init SumoSelect
	initSelect();
	
	// init inputmask
	initInputMask();
	
	//popup
	$body.on('click', '.c-popup-open', function (e) {
		e.preventDefault();
		PopUpShow($body);		
	});
	
	$body.on('click', '.c-popup-close', function (e) {
		e.preventDefault();
		 PopUpHide($body);		
	});
	
	//telephone show
	$body.on('click', '.c-tel', function (e) {
		e.preventDefault();
		
		var $telBtn = $('.c-tel');
		var	$telWrapper = $('.c-hidden-tel');
			
		$telBtn.toggleClass('active');
		if ($telBtn.hasClass('active')) {
			$telWrapper.show();
		} else {
			$telWrapper.hide();
		}
	});	
	
	//search show
	$body.on('click', '.c-search', function (e) {
		e.preventDefault();
		
		var $telBtn = $('.c-search');
		var	$telWrapper = $('.c-hidden-search');
			
		$telBtn.toggleClass('active');
		if ($telBtn.hasClass('active')) {
			$telWrapper.show();
		} else {
			$telWrapper.hide();
		}
	});
	
	
	limitedText($('.c-limited-text'), 80);	
	
	limitedText($('.c-limited-text1'), 50);
	
	openOrder();
	
	
	/*
	*	BEGIN navigation
	*/
	
	var $navLevels = $('.c-levels');	
	var $navLevel2 = $('.c-bl-level2');
	var $navLevel3 = $('.c-bl-level3');	
	var $hideMenuLev2 = $('.c-hidden-menu');
	var $hideMenuLev3 = $('.c-hidden-menu-l3');	
	
	var $navLevelsPopup = $('.c-popup-levels');
	var $navLevel2Popup = $('.c-popup-bl-level2');
	var $navLevel3Popup = $('.c-popup-bl-level3');
	var $hideMenuLev2Popup = $('.c-popup-hidden-menu');
	var $hideMenuLev3Popup = $('.c-popup-hidden-menu-l3');		
	
	// показываем второй уровень меню	
	$('body').on('click', '.c-level1', function () {			
		MenuLevel2Show ($(this), $navLevels, $navLevel2, $hideMenuLev2);		
	});	
	
	// показываем третий уровень меню	
	$('body').on('click', '.c-level2', function () {		
		MenuLevel3Show ($(this), $navLevels, $navLevel3, $hideMenuLev3);
	});		
	
	// возвращаемся к первому уровню меню
	$body.on('click', '.c-to-level1', function () {			
		MenuLevel2Close($navLevels);		
	});
	
	// возвращаемся к второму уровню меню
	$body.on('click', '.c-to-level2', function () {			
		MenuLevel3Close($navLevels, $navLevel2);		
	});	
	
	/*
	*	 block popup navigation
	*/	
	
	// показываем второй уровень меню	
	$('body').on('click', '.c-popup-level1', function () {			
		MenuLevel2Show ($(this), $navLevelsPopup, $navLevel2Popup, $hideMenuLev2Popup);		
	});	
	
	// показываем третий уровень меню	
	$('body').on('click', '.c-popup-level2', function () {		
		MenuLevel3Show ($(this), $navLevelsPopup, $navLevel3Popup, $hideMenuLev3Popup);
	});		
	
	// возвращаемся к первому уровню меню
	$body.on('click', '.c-popup-to-level1', function () {			
		MenuLevel2Close($navLevelsPopup);		
	});
	
	// возвращаемся к второму уровню меню
	$body.on('click', '.c-popup-to-level2', function () {			
		MenuLevel3Close($navLevelsPopup, $navLevel2Popup);		
	});	
	
	/*
	*	END navigation
	*/
	
	//tabs
	$body.on('click', '.c-tabs-link', function(e){
		e.preventDefault();

		var $link = $(this),
			$tabs = $link.closest('.c-tabs'),
			$content = $tabs.find('.c-tabs-content');
		
		$link.toggleClass('active').siblings().removeClass('active');
		
		if($link.hasClass('active')) {
			$content.removeClass('active');
			$content.eq($link.index()).addClass('active');
		} else {			
			$content.eq($link.index()).removeClass('active');
		}
		
		//Фильтр каталога
		if($('.c-filter-catalog').hasClass('active')) {
			$tabs.addClass('filter-catalog');
			$body.addClass('filter-active');
			$('.c-hidden').hide();
			
			$('.c-accordion-link').on('click', function(e) {
				e.preventDefault();
				$('.c-fixed').addClass('fixed');
				$('.c-filter').addClass('form--filter-fixed');
			});
			
		} else {
			$tabs.removeClass('filter-catalog');
			$('.c-hidden').show();
			$('.c-fixed').removeClass('fixed');
			$('.c-filter').removeClass('form--filter-fixed');
			$body.removeClass('filter-active');
		}
		
	});
	
	window.onscroll = function() {
		if($body.hasClass('filter-active')) {
			if($body.scrollTop() > 60) {
				$('.c-fixed').addClass('fixed');
				$('.c-filter').addClass('form--filter-fixed');
			} else {
				$('.c-fixed').removeClass('fixed');
				$('.c-filter').removeClass('form--filter-fixed');
			}
			
		}		
	};
	
	$body.on('touchstart', '.c-sort-btn', function(){
		$(this).toggleClass('up');
	});
	
	// accordion	
	showAccordion();
	
	// update orders
	updateOrders();
	
	
	// categories - anchor
	var $categoryLinks = $('.c-category');
	goCategoryAnchors($categoryLinks);
});